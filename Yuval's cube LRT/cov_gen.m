function [c,b]=cov_gen(a)
% COV_GEN
% [c,b]=cov_gen(a)
% a is a 3D matrix. cov_gen finds the covariance matrix in the 3rd dimention (depth).
% c is the covariance matrix, b is the pixels in columns.

[ra,ca,sa] = size(a);
a_flat     = reshape(a,[ra ca*sa]);
b          = im2col(a_flat,[ra ca],'distinct')'; % each column is a pixel, each row is a channel 
c          = cov(b');                            %  1 - normalize by N, 0 - normalize by N-1;
