%function [PC]=pc_gen
file_name=input('Enter file name to read = ','s');
data=enviread_new(file_name);
dmax=max(data(:))
data=data/dmax*255; 
disp('data=data/max(data)*255, DO NOT COMPARE TO RUNS DATED BEFORE 1/4/03!!')
[rpic, cpic, spic]=size(data); % pic size
[CM pic_in_row]=cov_gen(data); % CM = covariance matrix over the spectrum,...
															 % pic_in_row=each line is a picture, 
[V,S]=eigs(CM,spic);
Y=S^(-0.5)*V'*pic_in_row; % each picture is still a row.
PC=reshape(Y',rpic,cpic,spic); % reshaping into pictures
disp6(PC)