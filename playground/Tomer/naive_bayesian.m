clear all
% close all



components = 7;
max_iterations = 1000;
p = 0.03;
%% read the cube x
My_cube = enviread_new('bimodal.dat');
total_m = mean(mean(My_cube));
k(:) = total_m(1,1,:);
% figure;plot(k);
[rows,columns,bands] = size(My_cube);

%% calc x-m
mean_filter = (1/8)*[1,1,1;1,0,1;1,1,1];
meaned_cube = imfilter(My_cube,mean_filter,'symmetric');
meaned_cube(1:rows - 1:rows,:,:) = 0;meaned_cube(:,1:columns - 1:columns,:) = 0;
edged_mean = get_edged_mean(My_cube);
meaned_cube = edged_mean + meaned_cube;
x_minus_m_matrix = My_cube - meaned_cube;
target(:) = My_cube(5,3,:);
%% creating univerate pdf

for i = 1:bands
    temp_band = x_minus_m_matrix(:,:,i);temp_band = reshape(temp_band,[rows*columns 1]);
    channel_univerate_pdf{i} = fitgmdist(temp_band,components, 'Options',statset('MaxIter',max_iterations));
end

%% results_naive_bayesian_NT

%%%%%%need to check%%%%%%%%
pixels_NT = reshape(permute(x_minus_m_matrix,[3 2 1]),bands,[]);
% a_flat     = reshape(My_cube,[rows columns*bands]);
% pixels_NT          = im2col(a_flat,[rows columns],'distinct')'; % each column is a pixel, each row is a channel 
%%%%%%need to check%%%%%%%%

for i = 1:bands
    probability_per_pixel_NT_x_minus_pt(i,:) = channel_univerate_pdf{i}.pdf(pixels_NT(i,:)' - p * target(i));
    probability_per_pixel_NT_x(i,:) = channel_univerate_pdf{i}.pdf(pixels_NT(i,:)');
end
%throwing all the zeros
% probability_per_pixel_NT_x_minus_pt(probability_per_pixel_NT_x_minus_pt == 0) = 1;
% probability_per_pixel_NT_x(probability_per_pixel_NT_x == 0) = 1;


log_probability_per_pixel_NT_x = log10(probability_per_pixel_NT_x);
results_naive_bayesian_NT_x = sum(log_probability_per_pixel_NT_x,1);
results_naive_bayesian_NT_x = reshape(results_naive_bayesian_NT_x,[rows columns]);

log_probability_per_pixel_NT_x_minus_pt = log10(probability_per_pixel_NT_x_minus_pt);
results_naive_bayesian_NT_x_minus_pt = sum(log_probability_per_pixel_NT_x_minus_pt);
results_naive_bayesian_NT_x_minus_pt = reshape(results_naive_bayesian_NT_x_minus_pt,[rows columns]);

results_NT = results_naive_bayesian_NT_x_minus_pt - results_naive_bayesian_NT_x;

%% results_naive_bayesian_WT

My_cube_with_target = x_minus_m_matrix  + p * My_cube(5,3,:);
pixels_WT = reshape(permute(My_cube_with_target,[3 2 1]),bands,[]);

for i = 1:bands
    probability_per_pixel_WT_x_minus_pt(i,:) = channel_univerate_pdf{i}.pdf(pixels_WT(i,:)'  - p * target(i)); 
    probability_per_pixel_WT_x(i,:) = channel_univerate_pdf{i}.pdf(pixels_WT(i,:)');
end
%throwing all the zeros
% probability_per_pixel_WT_x_minus_pt(probability_per_pixel_WT_x_minus_pt == 0) = 1;
% probability_per_pixel_WT_x(probability_per_pixel_WT_x == 0) = 1;

log_probability_per_pixel_WT_x = log10(probability_per_pixel_WT_x);
results_naive_bayesian_WT_x = sum(log_probability_per_pixel_WT_x);
results_naive_bayesian_WT_x = reshape(results_naive_bayesian_WT_x,[rows columns]);

log_probability_per_pixel_WT_x_minus_pt = log10(probability_per_pixel_WT_x_minus_pt);
results_naive_bayesian_WT_x_minus_pt = sum(log_probability_per_pixel_WT_x_minus_pt);
results_naive_bayesian_WT_x_minus_pt = reshape(results_naive_bayesian_WT_x_minus_pt,[rows columns]);


results_WT = results_naive_bayesian_WT_x_minus_pt - results_naive_bayesian_WT_x;
[p_d , p_fa] = ROC(results_NT, results_WT);
figure;plot(p_fa,p_d);
trapz(p_fa,p_d)
xlim([0 0.1]);



