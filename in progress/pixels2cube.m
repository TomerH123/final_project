function cube = pixels2cube(pixels,M,N)
% convert rows of pixels to MxNxChannels hyperspectral cube

[num_pixels,num_channels] = size(pixels);

line_layers = reshape(pixels,num_pixels,1,num_channels);
cube        = reshape(line_layers,M,N,num_channels);
