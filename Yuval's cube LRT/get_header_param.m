function [samples, lines, bands, data_type, interleave]=get_header_param(header_file);
fid=fopen(header_file,'r');
t=fgetl(fid);
while t~=-1
    i=findstr('samples',t);
    if isempty(i)~=1
        samples=t(11:length(t));
        samples=str2double(samples);
    end
    i=findstr('lines',t);
    if isempty(i)~=1
        lines=t(11:length(t));
        lines=str2double(lines);
    end
    i=findstr('bands',t);
    if isempty(i)~=1
        bands=t(11:length(t));
        bands=str2double(bands);
    end
    i=findstr('data type',t);
    if isempty(i)~=1
        data_type=t(length(t));
        data_type=str2double(data_type);
    end
    i=findstr('interleave',t);
    if isempty(i)~=1
        interleave=t(14:length(t));
    end
    t=fgetl(fid);

end
fclose(fid);
        
        