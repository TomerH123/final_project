clear all
close all

%% read the cube and present avrage of the bands
My_cube = enviread_new('bimodal.dat');
total_m = mean(mean(My_cube));
k(:) = total_m(1,1,:);
% figure;plot(k);
[rows,columns,bands] = size(My_cube);

%% calc x'-m
mean_filter = (1/8)*[1,1,1;1,0,1;1,1,1];
meaned_cube = imfilter(My_cube,mean_filter,'symmetric');
meaned_cube(1:49:50,:,:) = 0;meaned_cube(:,1:49:50,:) = 0;
edged_mean = get_edged_mean(My_cube);
meaned_cube = edged_mean + meaned_cube;
x_minus_m_matrix = My_cube - meaned_cube;

%% calc phi and results without target and with target -  MF
pixels = reshape(permute(x_minus_m_matrix,[3 2 1]),bands,[]);
cov_matrix = cov((pixels)');
target(:) = My_cube(5,3,:);
target_absent = target / cov_matrix * pixels;
results_NT = reshape(target_absent,[rows columns]);
results_WT = get_results_WT(x_minus_m_matrix, cov_matrix, My_cube(5,3,:), 0.01);

%%create the ROC curve
[p_d , p_fa] = ROC(results_NT, results_WT);
figure;plot(p_fa,p_d,[0:0.1:1],[0:0.1:1]);


%% creating univerate pdf

for i = 1:bands
    temp_band = My_cube(:,:,i);temp_band = reshape(temp_band,[rows*columns 1]);
    channel_univerate_pdf{i} = fitgmdist(temp_band,2);
end

%% results_naive_beasian_NT

pixels_NT = reshape(permute(My_cube,[3 2 1]),bands,[]);
for i = 1:bands
    probability_per_pixel_NT(i,:) = channel_univerate_pdf{i}.pdf(pixels_NT(i,:)');
end
results_naive_beasian_NT = prod(probability_per_pixel_NT);
results_naive_beasian_NT = reshape(results_naive_beasian_NT,[rows columns]);


%% results_naive_beasian_WT
My_cube_with_target = My_cube + My_cube(5,3,:);

pixels_WT = reshape(permute(My_cube_with_target,[3 2 1]),bands,[]);
for i = 1:bands
    probability_per_pixel_WT(i,:) = channel_univerate_pdf{i}.pdf(pixels_WT(i,:)');
end
results_naive_beasian_WT = prod(probability_per_pixel_WT);
results_naive_beasian_WT = reshape(results_naive_beasian_NT,[rows columns]);

[p_d1 , p_fa1] = ROC(results_naive_beasian_NT, results_naive_beasian_WT);
figure;plot(p_fa1,p_d1,[0:0.1:1],[0:0.1:1]);





