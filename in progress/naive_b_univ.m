%% open cube and truncate 

cube = enviread_new('self_test_rad.img'); %bimodal.dat
 
data = cube;%(1:100,350:450,1:100);

%% general initializations

% ------------------------ Parameters ------------------------ %

p              = 5E-1; % target intensity
max_p_fa       = 2E-2; % ROC Pfa axis limit

components     = 1;    % number of GMM components
max_iterations = 500;  % max# of GMM iterations

hist_bins      = 1000; % number of bins in histograms

show_hists     = true;
show_CCDFs     = true;
show_ROC       = true;
set_model      = 'additive';    % 'substitutive' or 'additive'

mean_mode      = 'subtract'; %'keep' % use pixels with mean subtracted

% ------------------------------------------------------------ %

[rows,cols,bands] = size(data);

t_3d    = data(7,3,:);              % this is the target pixel
t       = reshape(t_3d,[bands 1]);  % rearranged as 2D vec

% average over neighbors and subtract
im_mean      = cube_mean(data);
im_min_m     = data - im_mean;

% data whitening options
pixels       = cube2pixels(data);     % original pixels
pixels_min_m = cube2pixels(im_min_m); % with mean subtracted
pixels_mean  = cube2pixels(im_mean);  % original pixels

if strcmp(mean_mode,'keep')
    % PCA without mean subtraction
    [pca_pixels,pca_matrix]   = pixels2pca(pixels);    % PCA transformed pixels
elseif strcmp(mean_mode,'subtract')
    % PCA with mean subtraction
    [pca_pixels,pca_matrix]   = pixels2pca(pixels_min_m); % PCA transformed pixels
end

mean_pixels_pca = pixels_mean * pca_matrix; % used when not subtracting mean 

% random traget (with similar scale power) 
t = (t'*pca_matrix)';

%% LRT - naive bayesian

% note: still getting slightly different results each time due to 
%       random initial conditions of GMM generation

disp 'generating LRT results'

% which pixels to use:
LRT_pixels = pca_pixels;

% generate GMM distributaions
if components == 1 % attept  - centered PDF
    gmm_arr = fitgmdist(LRT_pixels,components,...
        'Options',statset('MaxIter',max_iterations));
else % still with non-centered PDF 
    gmm_arr = {};
    for i = 1:bands
       gmm_arr{i} = fitgmdist(LRT_pixels(:,i),components,...
           'Options',statset('MaxIter',max_iterations));
    end
end

if components > 1
    
    if strcmp(set_model,'substitutive')
        t = t - mean_pixels_pca'; % this is a NumOfBandsbands x NumOfPixels
                                  % matrix since the mean is per pixel
    end
    
    %target absent p(x-pt|H0) & p(x|H0)
    f_x_H1_TA_channel = zeros(bands,rows*cols);
    f_x_H0_TA_channel = zeros(bands,rows*cols);
    for i=1:bands
        f_x_H1_TA_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i)-p*t(i))';
        f_x_H0_TA_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i))';
    end

    % log probabilities for pixel TA scores
    f_x_H1_TA = sum(log(f_x_H1_TA_channel),1);
    f_x_H0_TA = sum(log(f_x_H0_TA_channel),1);

    %target present p(x-pt|H0) & p(x|H0)
    f_x_H1_TP_channel = zeros(bands,rows*cols);
    f_x_H0_TP_channel = zeros(bands,rows*cols);
    for i=1:bands
        f_x_H1_TP_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i))';
        f_x_H0_TP_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i)+p*t(i))';
    end
    % log probabilities for pixel TP scores
    f_x_H1_TP = sum(log(f_x_H1_TP_channel),1);
    f_x_H0_TP = sum(log(f_x_H0_TP_channel),1);
    
else
    
end

% LRT scores (logarithmic)
LRT_TA    = f_x_H1_TA-f_x_H0_TA;
LRT_TP    = f_x_H1_TP-f_x_H0_TP;

%results
LRT_res_TA = reshape(LRT_TA,[rows,cols]);
LRT_res_TP = reshape(LRT_TP,[rows,cols]);

%histograms
[LRT_TA_hist,LRT_TA_vals] = gen_hist(LRT_res_TA,hist_bins);
[LRT_TP_hist,LRT_TP_vals] = gen_hist(LRT_res_TP,hist_bins);

% figure; plot(LRT_TA_vals,LRT_TA_hist); grid; hold; plot(LRT_TP_vals,LRT_TP_hist);

%CCDF
norm_LRT_TA_hist = LRT_TA_hist/sum(LRT_TA_hist);
norm_LRT_TP_hist = LRT_TP_hist/sum(LRT_TP_hist);
LRT_TA_CCDF      = 1-cumsum(norm_LRT_TA_hist);
LRT_TP_CCDF      = 1-cumsum(norm_LRT_TP_hist);

% figure; plot(LRT_TA_vals,LRT_TA_CCDF); grid; hold; plot(LRT_TP_vals,LRT_TP_CCDF);

%ROC
LRT_extend = length(LRT_TP_vals(LRT_TP_vals>max(LRT_TA_vals)));
LRT_P_d    = [ones(1,LRT_extend) LRT_TP_CCDF];
LRT_P_fa   = [LRT_TA_CCDF zeros(1,LRT_extend)];

% figure; plot(LRT_P_fa(LRT_P_fa<max_p_fa),LRT_P_d(LRT_P_fa<max_p_fa)); grid;

%% MF (for comparison)

disp 'generating MF results'

MF_cube = pixels2cube(pca_pixels,rows,cols); % use pca_cube

% average over neighbors and subtract
[phi,MF_pixels] = cov_gen(MF_cube);  % cov of X - m

% MF:
MF_flat_TA = t'/phi*(MF_pixels);     % target absent
MF_flat_TP = t'/phi*(MF_pixels+p*t); % target present

%results
MF_res_TA = reshape(MF_flat_TA,[rows,cols]);
MF_res_TP = reshape(MF_flat_TP,[rows,cols]);

%histograms
[MF_TA_hist,MF_TA_vals] = gen_hist(MF_res_TA,hist_bins);
[MF_TP_hist,MF_TP_vals] = gen_hist(MF_res_TP,hist_bins);

% figure; plot(MF_TA_vals,MF_TA_hist); grid; hold; plot(MF_TP_vals,MF_TP_hist);

%CCDF
norm_MF_TA_hist = MF_TA_hist/sum(MF_TA_hist);
norm_MF_TP_hist = MF_TP_hist/sum(MF_TP_hist);
MF_TA_CCDF      = 1-cumsum(norm_MF_TA_hist);
MF_TP_CCDF      = 1-cumsum(norm_MF_TP_hist);

% figure; plot(MF_TA_vals,MF_TA_CCDF); grid; hold; plot(MF_TP_vals,MF_TP_CCDF);

% ROC
MF_extend = length(MF_TP_vals(MF_TP_vals>max(MF_TA_vals)));
MF_P_d    = [ones(1,MF_extend) MF_TP_CCDF];
MF_P_fa   = [MF_TA_CCDF zeros(1,MF_extend)];

% figure; plot(MF_P_fa(MF_P_fa<max_p_fa),MF_P_d(MF_P_fa<max_p_fa)); grid;

%% compare MF to LRT

if show_hists
    figure;
    subplot('position',[0.03 0.05 0.45 0.9]);
    plot(MF_TA_vals,MF_TA_hist); grid; hold;   plot(MF_TP_vals,MF_TP_hist);   title({'MF histograms',['P = ' num2str(p)]});  legend('Target absent','Target present');
    subplot('position',[0.52  0.05 0.45 0.9]);
    plot(LRT_TA_vals,LRT_TA_hist); grid; hold; plot(LRT_TP_vals,LRT_TP_hist); title({'LRT histograms',strcat('P = ',num2str(p),', Num of gmm components = ',num2str(components))}); legend('Target absent','Target present');
end

if show_CCDFs
    figure;
    subplot('position',[0.03 0.05 0.45 0.9]);
    plot(MF_TA_vals,MF_TA_CCDF);   grid; hold; plot(MF_TP_vals,MF_TP_CCDF);   title({'MF CCDFs',['P = ',num2str(p)]});  legend('Target absent','Target present'); 
    subplot('position',[0.52  0.05 0.45 0.9]);
    plot(LRT_TA_vals,LRT_TA_CCDF); grid; hold; plot(LRT_TP_vals,LRT_TP_CCDF); title({'LRT CCDFs',['P = ',num2str(p),', Num of gmm components = ',num2str(components)]}); legend('Target absent','Target present');
end

if show_ROC
    figure;
    plot(MF_P_fa,MF_P_d); hold;
    plot(LRT_P_fa,LRT_P_d);
    grid; legend('MF ROC','LRT ROC'); xlim([0 max_p_fa]);title({'ROC Curce LRT VS MF',['P = ',num2str(p),', Num of gmm components = ',num2str(components)]});
end

%% compare original layers to PCA layers

% pca_data = pixels2cube(pca_pixels,rows,cols);
% 
% figure;
% for i = 1:16
%     subplot(4,4,i); imshow(pca_data(:,:,i),[]);
% end
% 
% figure;
% for i = 1:16
%     subplot(4,4,i); imshow(data(:,:,i),[]);
% end
