function [c,b]=cov_gen(a)
% COV_GEN
% [c,b]=cov_gen(a)
% a is a 3D matrix. cov_gen finds the covariance matrix in the 3rd dimention (depth).
% c is the covariance matrix, b is the pictures in rows.

disp('Calculating covariance matrix');
[ra,ca,sa]=size(a);
b=zeros(sa,ra*ca);
b=im2col(a,[ra ca],'d')'; % pictures in rows, spectrums in columns. 
c=cov(b'); % The covariance is calculated between colomns, so we use b' - pictures in columns.