function [hist,vals] = gen_hist(img,nbins)
% normalize to use imhist then rescale

im_max   = max(img(:));
im_min   = min(img(:));

%nomalize and get hist
norm_img    = (img-im_min)/(im_max-im_min);
[hist,bins] = imhist(norm_img,nbins);

% rescale
vals = bins'*(im_max - im_min) + im_min;
hist = hist';