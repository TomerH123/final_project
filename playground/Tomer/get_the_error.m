function error = get_the_error(I1,I2)
%this function returns the error diffrence between two matrices, according to
%sum(I1(:) - I2(:)), norm, max(abs(I1-I2)), any dimension is good
disp("the norm of the difference between the matrixes is:")
error = norm(I1(:) - I2(:))
disp("the sum of the difference between the matrixes is:")
error = sum(I1(:) - I2(:))
disp("the max difference in abs between the matrixes is:")
error = max(abs(I1(:) - I2(:)))
disp("number of diffrenet cells:")
error = sum(I1(:)~=I2(:))
end