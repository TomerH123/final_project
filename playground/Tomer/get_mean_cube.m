function mean_cube = get_mean_cube(cube)

    [rows,columns,bands] = size(cube);
    mean_filter = (1/8)*[1,1,1;1,0,1;1,1,1];
    meaned_cube = imfilter(cube,mean_filter,'symmetric');
    meaned_cube(1:rows - 1:rows,:,:) = 0;meaned_cube(:,1:columns - 1:columns,:) = 0;
    edged_mean = get_edged_mean(cube);
    mean_cube = edged_mean + meaned_cube;

end