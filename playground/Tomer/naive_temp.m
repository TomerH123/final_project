clear all
% close all



components = 3;
max_iterations = 500;
p = 0.01;
%% read the cube x

My_cube = enviread_new('bimodal.dat');
[rows,columns,bands] = size(My_cube);
target(:) = My_cube(5,3,:);

%% calc x-m

meaned_cube = get_mean_cube(My_cube);
x_min_m = My_cube - meaned_cube;

%% pca

pixels = reshape(permute(x_min_m,[3 2 1]),bands,[])';
coeff = pca(pixels);
whiten = pixels * coeff;

%% creating univerate pdf

for i = 1:bands
    temp_band = x_min_m(:,:,i);temp_band = reshape(temp_band,[rows*columns 1]);
    channel_univerate_pdf{i} = fitgmdist(whiten(:,i),components, 'Options',statset('MaxIter',max_iterations));
end

%% results_naive_bayesian_NT

%%%%%%need to check%%%%%%%%
% pixels_NT = reshape(permute(x_minus_m_matrix,[3 2 1]),bands,[]);
% a_flat     = reshape(My_cube,[rows columns*bands]);
% pixels_NT          = im2col(a_flat,[rows columns],'distinct')'; % each column is a pixel, each row is a channel 
%%%%%%need to check%%%%%%%%

for i = 1:bands
    probability_per_pixel_NT_x_minus_pt(i,:) = channel_univerate_pdf{i}.pdf(whiten(:,i) - p * target(i))';
    probability_per_pixel_NT_x(i,:) = channel_univerate_pdf{i}.pdf(whiten(:,i))';
end
%throwing all the zeros
% probability_per_pixel_NT_x_minus_pt(probability_per_pixel_NT_x_minus_pt == 0) = 1;
% probability_per_pixel_NT_x(probability_per_pixel_NT_x == 0) = 1;


log_probability_per_pixel_NT_x = log10(probability_per_pixel_NT_x);
results_naive_bayesian_NT_x = sum(log_probability_per_pixel_NT_x,1);
results_naive_bayesian_NT_x = reshape(results_naive_bayesian_NT_x,[rows columns]);

log_probability_per_pixel_NT_x_minus_pt = log10(probability_per_pixel_NT_x_minus_pt);
results_naive_bayesian_NT_x_minus_pt = sum(log_probability_per_pixel_NT_x_minus_pt);
results_naive_bayesian_NT_x_minus_pt = reshape(results_naive_bayesian_NT_x_minus_pt,[rows columns]);

results_NT = results_naive_bayesian_NT_x_minus_pt - results_naive_bayesian_NT_x;

%% results_naive_bayesian_WT

% My_cube_with_target = x_minus_m_matrix  + p * My_cube(5,3,:);
% pixels_WT = reshape(permute(My_cube_with_target,[3 2 1]),bands,[]);

for i = 1:bands
    probability_per_pixel_WT_x_minus_pt(i,:) = channel_univerate_pdf{i}.pdf(whiten(:,i))'; 
    probability_per_pixel_WT_x(i,:) = channel_univerate_pdf{i}.pdf(whiten(:,i)  + p * target(i))';
end
%throwing all the zeros
% probability_per_pixel_WT_x_minus_pt(probability_per_pixel_WT_x_minus_pt == 0) = 1;
% probability_per_pixel_WT_x(probability_per_pixel_WT_x == 0) = 1;

log_probability_per_pixel_WT_x = log10(probability_per_pixel_WT_x);
results_naive_bayesian_WT_x = sum(log_probability_per_pixel_WT_x);
results_naive_bayesian_WT_x = reshape(results_naive_bayesian_WT_x,[rows columns]);

log_probability_per_pixel_WT_x_minus_pt = log10(probability_per_pixel_WT_x_minus_pt);
results_naive_bayesian_WT_x_minus_pt = sum(log_probability_per_pixel_WT_x_minus_pt);
results_naive_bayesian_WT_x_minus_pt = reshape(results_naive_bayesian_WT_x_minus_pt,[rows columns]);


results_WT = results_naive_bayesian_WT_x_minus_pt - results_naive_bayesian_WT_x;
[p_d , p_fa] = ROC(results_NT, results_WT);
figure;plot(p_fa,p_d);
trapz(p_fa,p_d)
xlim([0 0.1]);



