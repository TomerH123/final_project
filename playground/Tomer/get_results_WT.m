function results_WT = get_results_WT(x_minus_m_matrix, phi, target, p)
%this function recieves x-m matrix, phi, target signture, and target
%intensity and returns a matrix of matched filter results
%tarminoligy - x' = x-pt, the matched filter is: t/phi*(x'-m)

target_vector(:) = target;
target_is_present = x_minus_m_matrix + p * target;
pixels = reshape(permute(target_is_present,[3 2 1]),91,[]);
target_present = target_vector / phi * pixels;
results_WT = reshape(target_present,[50 50]);

end