function pixels = cube2pixels(cube)
% each row is a pixel, each column is a channel

[rows,cols,layers] = size(cube);
cube_flat          = reshape(cube,[rows cols*layers]);
pixels             = im2col(cube_flat,[rows cols],'distinct'); 