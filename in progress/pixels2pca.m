function [pca_pixels,eig_vecs] = pixels2pca(pixels)
% each row is a pixel, each column is a channel
% by default 
%[coeff,score,latent,tsquared,explained,mu] - outputs of pca

[eig_vecs,pca_pixels,latent,tsquared,explained,mu]   = pca(pixels);
% pca_pixels = pixels*eig_vecs;