
function m = cube_mean(cube)

% m is a 2D matrix which contains the mean value of each pixel in 'cube' 
% the mean is based on 8, 5 or 3 neighbors. (central, edges and corners)

central_mean  = imfilter(cube,[1 1 1;1 0 1;1 1 1]/8); 

% edges
up_mean       = imfilter(cube(1:2,:,:),[0 0 0;1 0 1;1 1 1]/5); 
down_mean     = imfilter(cube(end-1:end,:,:),[1 1 1;1 0 1;0 0 0]/5); 
left_mean     = imfilter(cube(:,1:2,:),[0 1 1;0 0 1;0 1 1]/5);
right_mean    = imfilter(cube(:,end-1:end,:),[1 1 0;1 0 0;1 1 0]/5);

% corners
top_left      = (cube(1,2,:) + cube(2,1,:) + cube(2,2,:))/3;
top_right     = (cube(1,end,:) + cube(2,end-1,:) + cube(2,end,:))/3;
bottom_left   = (cube(end-1,1,:) + cube(end-1,2,:) + cube(end,2,:))/3;
bottom_right  = (cube(end-1,end-1,:) + cube(end-1,end,:) + cube(end,end-1,:))/3;

% build m
m             = central_mean;
m(1,:,:)      = up_mean(1,:,:);
m(end,:,:)    = down_mean(2,:,:);
m(:,1,:)      = left_mean(:,1,:);
m(:,end,:)    = right_mean(:,end,:);
m(1,1,:)      = top_left;
m(1,end,:)    = top_right;
m(end,1,:)    = bottom_left;
m(end,end,:)  = bottom_right;