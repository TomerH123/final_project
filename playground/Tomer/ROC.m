function [p_d , p_fa] = ROC(NT, WT)
%this function recieves 2 matrices, the first one NT, represents the
%result matrix of a target detection test with no target (each cell is a score of a pixel)
%, and WT, represents the result matrix of a target detection test with target present 
%(each cell is a score of a pixel being the target)

min_of_results = min(min(NT(:)),min(WT(:)));
max_of_results = max(max(NT(:)),max(WT(:)));
figure;
hist_NT = histogram(NT(:),linspace(min_of_results,max_of_results));
hist_NT = hist_NT.Values;
title("target absent")
figure
hist_WT = histogram(WT(:),linspace(min_of_results,max_of_results));
hist_WT = hist_WT.Values;
title("target present")
hist_NT_normalizied = hist_NT/numel(NT);
hist_WT_normalizied = hist_WT/numel(WT);
cum_NT = cumsum(hist_NT_normalizied);
cum_WT = cumsum(hist_WT_normalizied);
Ccum_NT = 1 - cum_NT;
Ccum_WT = 1- cum_WT;
figure; plot(Ccum_NT);hold on; plot(Ccum_WT);
p_d  = Ccum_WT;
p_fa = Ccum_NT;
end















