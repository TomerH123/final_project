function meaned_edges = get_edged_mean(I)
%%this function recieves a normlized image (any 3d dimensions), and returns
%%the mean of the edges 5 neighbors or 3 (at the corners - the middle will
%%be zero.
[rows,columns,bands] = size(I);
meaned_edges = imfilter(I,(1/5)*[1,1,1;1,0,1;1,1,1]);
meaned_edges(2:(rows-1),2:(columns-1),:) = 0;
%fixing corners
meaned_edges(1,1,:) = (I(1,2,:) + I(2,2,:) + I(2,1,:))/3;
meaned_edges(rows,1,:) = (I(rows-1,1,:) + I(rows-1,2,:) + I(rows,2,:))/3;
meaned_edges(1,columns,:) = (I(1,columns - 1,:) + I(2,columns - 1,:) + I(2,columns,:))/3;
meaned_edges(rows,columns,:) = (I(rows - 1,columns,:) + I(rows,columns - 1,:) + I(rows - 1,columns - 1,:))/3;
end