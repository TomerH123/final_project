data = enviread_new('bimodal.dat');

% The algorithm success is highly dependent on starting condition

components     = 8;
max_iterations = 5000;

t_3d    = data(5,3,:);           % this is the target pixel
t       = reshape(t_3d,[91 1]);  % rearranged as 2D vec

% plant target
p = 0.01; % intensity

% average over neighbors and subtract
im_mean      = cube_mean(data);
im_min_m     = data - im_mean;

% LRT:
pixels    = flat_cube(data);

% gmm_dist  = fitgmdist(pixels,components,...
% ...%             'CovarianceType','diagonal');%,...
%             'Options',statset('MaxIter',max_iterations));


%target absent
f_x_H1_TA = gmm_dist.pdf(pixels-p*t');
f_x_H0_TA = gmm_dist.pdf(pixels);
LRT_TA    = f_x_H1_TA./f_x_H0_TA;

%target present
f_x_H1_TP = gmm_dist.pdf(pixels);
f_x_H0_TP = gmm_dist.pdf(pixels+p*t');
LRT_TP    = f_x_H1_TP./f_x_H0_TP;

%results
LRT_res_TA = reshape(LRT_TA,[50,50]);
LRT_res_TP = reshape(LRT_TP,[50,50]);

%histograms
[TA_hist,TA_vals] = gen_hist(log(LRT_res_TA),100);
[TP_hist,TP_vals] = gen_hist(log(LRT_res_TP),100);


%CCDF
norm_TA_hist = TA_hist/sum(TA_hist);
norm_TP_hist = TP_hist/sum(TP_hist);
TA_CCDF      = 1-cumsum(norm_TA_hist);
TP_CCDF      = 1-cumsum(norm_TP_hist);

%ROC
extend = length(TP_vals(TP_vals>max(TA_vals)));
P_d    = [ones(1,extend) TP_CCDF];
P_fa   = [TA_CCDF zeros(1,extend)];

figure; plot(P_fa(P_fa<0.1),P_d(P_fa<0.1)); grid; % for CFAR = 0.1

