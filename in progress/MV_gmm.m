%% ------------------------ Comments ------------------------ %
%
%   - consider grouping channels and running GMM EM on groups, then
%   use product as pdf estimation
%
%
% ------------------------------------------------------------ %

%%

% close all
clear
% clc

%% open cube and truncate 

data = enviread_new('self_test_refl.img'); %bimodal.dat

%% general initializations

% ------------------------ Parameters ------------------------ %
params                 = {};

target_model           = 'substitutive'; % 'substitutive' or 'additive'

p               = 5E-1; % target intensity
max_p_fa        = 1E-1; % ROC Pfa axis limit
th              = max_p_fa;%1E-2; % for computing A_th

components      = 3;    % number of GMM components
max_iterations  = 500;  % max# of GMM iterations

hist_bins       = 5000; % number of bins in histograms
LRT_pixels_used = 'zero_mean_pca'; %'normal' , 'zero_mean' , 'pca' , 'zero_mean_pca'
MF_pixels_used  = 'zero_mean_pca'; %'normal' , 'zero_mean' , 'pca' , 'zero_mean_pca'

show_hists     = true;
show_CCDFs     = true;
show_ROC       = true;

use_MVgmm      = false;      % approx pdf using MV gmm model

save_results   = true;
% ------------------------------------------------------------ %

[rows,cols,bands] = size(data);

t_3d    = data(7,3,:);              % this is the target pixel
t       = reshape(t_3d,[1 bands]);  % rearranged as row vector

% average over neighbors and subtract
im_mean      = cube_mean(data);
im_min_m     = data - im_mean;

% arrange pixels in a 2D matrix
pixels       = cube2pixels(data);     % original pixels
pixels_min_m = cube2pixels(im_min_m); % with mean subtracted
pixels_mean  = cube2pixels(im_mean);  % means matrix

% PCA
[pca_pixels,pca_matrix] = pixels2pca(pixels);                            % PCA transformed pixels
[zero_mean_pca_pixels,zero_mean_pca_matrix] = pixels2pca(pixels_min_m);  % PCA transformed pixels

% which pixels to use:
switch LRT_pixels_used %'normal' , 'zero_mean' , 'pca' , 'zero_mean_pca'
    case 'normal'
        LRT_pixels = pixels;
        LRT_mean   = pixels_mean;
        LRT_target = t;
    case 'zero_mean'
        LRT_pixels = pixels_min_m;
        LRT_mean   = zeros(size(pixels_mean));
        LRT_target = t;
    case 'pca'
        LRT_pixels = pca_pixels;
        LRT_mean   = pixels_mean*pca_matrix;
        LRT_target = t*pca_matrix;
    case 'zero_mean_pca'
        LRT_pixels = zero_mean_pca_pixels;
        LRT_mean   = zeros(size(pixels_mean));
        LRT_target = t*zero_mean_pca_matrix;
end
switch MF_pixels_used %'normal' , 'zero_mean' , 'pca' , 'zero_mean_pca'
    case 'normal'
        MF_pixels  = pixels;
        MF_mean    = pixels_mean;
        MF_target  = t;
    case 'zero_mean'
        MF_pixels  = pixels_min_m;
        MF_mean    = zeros(size(pixels_mean));
        MF_target  = t;
    case 'pca'
        MF_pixels  = pca_pixels;
        MF_mean    = pixels_mean*pca_matrix;
        MF_target  = t*pca_matrix;
    case 'zero_mean_pca'
        MF_pixels  = zero_mean_pca_pixels;
        MF_mean    = zeros(size(pixels_mean));
        MF_target  = t*zero_mean_pca_matrix;
end

clear data pca_pixels pca_matrix zero_mean_pca_pixels zero_mean_pca_matrix 
clear pixels pixels_mean pixels_min_m im_mean im_min_m


%% LRT - naive bayesian

% note: still getting slightly different results each time due to 
%       random initial conditions of GMM generation

disp 'generating LRT results'

% empty pdf values arrays
%target absent
f_x_H1_TA_channel = zeros(bands,rows*cols);
f_x_H0_TA_channel = zeros(bands,rows*cols);
%target present
f_x_H1_TP_channel = zeros(bands,rows*cols);
f_x_H0_TP_channel = zeros(bands,rows*cols);


% generate GMM distributaions
if use_MVgmm % attept  - centered PDF
    gmm_arr = fitgmdist(LRT_pixels,components,...
        'ProbabilityTolerance',0,'Options',statset('MaxIter',max_iterations));
else % product of univariate GMMs
    gmm_arr = {};
    parfor i = 1:bands
        
        gmm_arr{i} = fitgmdist(LRT_pixels(:,i),components,...
           'ProbabilityTolerance',0,'Options',statset('MaxIter',max_iterations));
       
        if strcmp(target_model,'additive')
            
            %target absent p(x-pt|H0) & p(x|H0)
            f_x_H1_TA_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i)-p*LRT_target(i))';
            f_x_H0_TA_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i))';

            %target present p(x-pt|H0) & p(x|H0)
            f_x_H1_TP_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i))';
            f_x_H0_TP_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i)+p*LRT_target(i))';
            
        elseif strcmp(target_model,'substitutive')  
            
            %target absent p(x-pt|H0) & p(x|H0)
            f_x_H1_TA_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i) - p*(LRT_target(i) - LRT_mean(:,i)))';
            f_x_H0_TA_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i))';

            %target present p(x-pt|H0) & p(x|H0)
            f_x_H1_TP_channel(i,:) = gmm_arr{i}.pdf(LRT_pixels(:,i))';
            f_x_H0_TP_channel(i,:) = gmm_arr{i}.pdf((1-p)*LRT_pixels(:,i) + p*(LRT_target(i) - LRT_mean(:,i)));
        end
    end
end

% log probabilities for pixel TA scores
f_x_H1_TA = sum(log(f_x_H1_TA_channel),1);
f_x_H0_TA = sum(log(f_x_H0_TA_channel),1);

% log probabilities for pixel TP scores
f_x_H1_TP = sum(log(f_x_H1_TP_channel),1);
f_x_H0_TP = sum(log(f_x_H0_TP_channel),1);

% LRT scores (logarithmic)
LRT_TA    = f_x_H1_TA-f_x_H0_TA;
LRT_TP    = f_x_H1_TP-f_x_H0_TP;

% histograms
limits = [min([LRT_TA LRT_TP]) max([LRT_TA LRT_TP])];
[LRT_TA_hist,bins] = histcounts(LRT_TA,hist_bins,...
    'BinLimits',limits,'Normalization','probability');
[LRT_TP_hist,~] = histcounts(LRT_TP,hist_bins,...
    'BinLimits',limits,'Normalization','probability');
bins = bins(1:end-1)+(bins(2)-bins(1))/2;
LRT_TA_vals = bins; LRT_TP_vals = bins;

%CCDF
LRT_TA_CCDF      = 1-cumsum(LRT_TA_hist);
LRT_TP_CCDF      = 1-cumsum(LRT_TP_hist);

%ROC
LRT_P_d    = fliplr(LRT_TP_CCDF);
LRT_P_fa   = fliplr(LRT_TA_CCDF);

%% MF (for comparison)

disp 'generating MF results'

% covariance
phi       = cov(MF_pixels);
MF_pixels = MF_pixels'; %put pixels in columns
MF_mean   = MF_mean';

% MF:
if strcmp(target_model,'additive')
    MF_TA = MF_target/phi * ... 
                 (MF_pixels - MF_mean);                % target absent
    MF_TP = MF_target/phi * ...
                 (MF_pixels + p*MF_target' - MF_mean); % target present
             
elseif strcmp(target_model,'substitutive')
    MF_TA = sum(((MF_target-MF_mean')/phi)' .* ...
                 (MF_pixels - MF_mean),1);                    % target absent
    MF_TP = sum(((MF_target-MF_mean')/phi)' .* ...
                 ((1-p)*MF_pixels+p*MF_target' - MF_mean),1); % target present
end

% histograms
limits = [min([MF_TA MF_TP]) max([MF_TA MF_TP])];
[MF_TA_hist,bins] = histcounts(MF_TA,hist_bins,...
    'BinLimits',limits,'Normalization','probability');
[MF_TP_hist,~] = histcounts(MF_TP,hist_bins,...
    'BinLimits',limits,'Normalization','probability');
bins = bins(1:end-1)+(bins(2)-bins(1))/2;
MF_TA_vals = bins; MF_TP_vals = bins;

%CCDF
MF_TA_CCDF      = 1-cumsum(MF_TA_hist);
MF_TP_CCDF      = 1-cumsum(MF_TP_hist);

%ROC
MF_P_d    = fliplr(MF_TP_CCDF);
MF_P_fa   = fliplr(MF_TA_CCDF);

%% compare MF to LRT

results = {};

if show_hists
    hists_f = figure;
else
    hists_f = figure('visible', 'off');
end
subplot('position',[0.03 0.05 0.45 0.9]);
plot(MF_TA_vals,MF_TA_hist); grid; hold;   plot(MF_TP_vals,MF_TP_hist);   title({'MF histograms',['P = ' num2str(p)]});  legend('Target absent','Target present');
subplot('position',[0.52  0.05 0.45 0.9]);
plot(LRT_TA_vals,LRT_TA_hist); grid; hold; plot(LRT_TP_vals,LRT_TP_hist); title({'LRT histograms',strcat('P = ',num2str(p),', Num of gmm components = ',num2str(components))}); legend('Target absent','Target present');

F = getframe; hists = frame2im(F);

if show_CCDFs
    CCDFs_f = figure;
else
    CCDFs_f = figure('visible', 'off');
end
subplot('position',[0.03 0.05 0.45 0.9]); 
plot(MF_TA_vals,MF_TA_CCDF); ylim([0 1]); grid; hold; plot(MF_TP_vals,MF_TP_CCDF);   title({'MF CCDFs',['P = ',num2str(p)]});  legend('Target absent','Target present'); 
subplot('position',[0.52  0.05 0.45 0.9]); 
plot(LRT_TA_vals,LRT_TA_CCDF); ylim([0 1]); grid; hold; plot(LRT_TP_vals,LRT_TP_CCDF); title({'LRT CCDFs',['P = ',num2str(p),', Num of gmm components = ',num2str(components)]}); legend('Target absent','Target present');


F = getframe; CCDFs = frame2im(F);

if show_ROC
    ROCs_f = figure;
else
    ROCs_f = figure('visible', 'off');
end
plot(MF_P_fa,MF_P_d); hold;
plot(LRT_P_fa,LRT_P_d); 
grid; legend('MF ROC','LRT ROC'); xlim([0 max_p_fa]);title({'ROC Curce LRT VS MF',['P = ',num2str(p),', Num of gmm components = ',num2str(components)]});

F = getframe; ROCs = frame2im(F);

figures = {hists CCDFs ROCs};

%% compare Ath

%integration interval
LRT_x = LRT_P_fa(LRT_P_fa<=th);
LRT_y = LRT_P_d(LRT_P_fa<=th);
MF_x  = MF_P_fa(MF_P_fa<=th);
MF_y  = MF_P_d(MF_P_fa<=th);

% compute values
A_LRT = trapz(LRT_x,LRT_y); 
A_MF  = trapz(MF_x,MF_y); 

LRT_A_th = (A_LRT - 0.5*th^2)/(th - 0.5*th^2)
MF_A_th  = (A_MF - 0.5*th^2)/(th - 0.5*th^2)

%% save results

if save_results
    load results_arr
    switch target_model %'substitutive' or 'additive'
        case 'additive'
            results_arr.add(end+1,:) = {string(datetime) p max_p_fa th components max_iterations hist_bins LRT_pixels_used MF_pixels_used LRT_A_th MF_A_th figures};
        case 'substitutive'
            results_arr.sub(end+1,:) = {string(datetime) p max_p_fa th components max_iterations hist_bins LRT_pixels_used MF_pixels_used LRT_A_th MF_A_th figures};
    end
    save('results_arr','results_arr'); 
end



















    
    
    
    
