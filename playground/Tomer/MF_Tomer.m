clear all
close all

%% read the cube and present avrage of the bands
My_cube = enviread_new('bimodal.dat');
total_m = mean(mean(My_cube));
k(:) = total_m(1,1,:);
% figure;plot(k);
[rows,columns,bands] = size(My_cube);
p = 0.01;
%% calc x'-m
mean_filter = (1/8)*[1,1,1;1,0,1;1,1,1];
meaned_cube = imfilter(My_cube,mean_filter,'symmetric');
meaned_cube(1:49:50,:,:) = 0;meaned_cube(:,1:49:50,:) = 0;
edged_mean = get_edged_mean(My_cube);
meaned_cube = edged_mean + meaned_cube;
x_minus_m_matrix = My_cube - meaned_cube;

%% calc phi and results without target and with target -  MF
pixels = reshape(permute(x_minus_m_matrix,[3 2 1]),bands,[]);
cov_matrix = cov((pixels)');
target(:) = My_cube(5,3,:);
target_absent = target / cov_matrix * pixels;
results_NT = reshape(target_absent,[rows columns]);
results_WT = get_results_WT(x_minus_m_matrix, cov_matrix, My_cube(5,3,:), p);

%%create the ROC curve
[p_d , p_fa] = ROC(results_NT, results_WT);
figure;plot(p_fa,p_d,[0:0.1:1],[0:0.1:1]);
xlim([0 0.1]);

